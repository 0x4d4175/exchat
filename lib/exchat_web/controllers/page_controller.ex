defmodule ExchatWeb.PageController do
  use ExchatWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
