defmodule ExchatWeb.RoomChannel do
  use ExchatWeb, :channel

  def join("room:" <> room_name, payload, socket) do
    if authorized?(payload) do
      new_socket = assign(socket, :room, room_name)
      {:ok, new_socket}
    else
      {:error, %{reason: "unauthorized"}}
    end
  end

  #TODO: enable joining arbitrary rooms
  #TODO: persist messages 

  # Channels can be used in a request/response fashion
  # by sending replies to requests from the client
  # TODO: remove this, because unecessary
  def handle_in("ping", payload, socket) do
    {:reply, {:ok, payload}, socket}
  end

  # It is also common to receive messages from the client and
  # broadcast to everyone in the current topic (room:lobby).
  def handle_in("chat_msg", payload, socket) do
    broadcast(socket, "chat_msg", payload)
    # Send the message back to our selved to be in sync with everyone
    {:reply, {:ok, payload}, socket}
  end

  # Add authorization logic here as required.
  defp authorized?(_payload) do
    true
  end
end
