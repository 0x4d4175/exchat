import * as React from 'react'

type MessageData = {
    key: Number,
    name: String,
    message: String
}

export default class Message extends React.Component<MessageData> {
  public render(): JSX.Element {
    return (
        <div className="flex">
            <div className="w-32 px-1 inline-block font-bold">{this.props.name}</div>
            <div className="w-full px-1 inline-block">{this.props.message}</div>
        </div>
    )
  }
}