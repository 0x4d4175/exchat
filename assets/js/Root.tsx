import * as React from 'react'

import Chat from './Chat';
import Logon from './Logon';

type RootState = {
  channel: String
  name: String
  status: String
}

export default class Root extends React.Component<{}, RootState> {
  constructor(props:any) {
    super(props);

    this.state = {
      channel: "lobby",
      name: "",
      status: "loaded"
    }
  }

  private _onLogon = (username:string, channel:string) => {
    this.setState({...this.state, name: username, channel: channel, status: "loggedOn"});
  }

  private renderChat(): JSX.Element {
    return (
      <Chat name={this.state.name} channel={this.state.channel} />
    );
  }

  private renderLogon(): JSX.Element {
    return (
      <Logon logonCallback={this._onLogon} />
    );
  }

  private renderError(): JSX.Element {
    return (
      <div> ERROR! </div>
    )
  }

  public render(): JSX.Element {
    if (this.state.status === "loggedOn") {
      return this.renderChat();
    } else if (this.state.status === "loaded") {
      return this.renderLogon();
    } else {
      return this.renderError();
    }
  }
}