import * as React from 'react'

/* TODO: create type declaration for phoenix */
import {Socket} from "phoenix";

import Message from './Message';

type ChatProps = {
  name: String,
  channel: String
}

type ChatMessage = {
    name: String,
    message: String
}

type ChatState = {
    messages: Array<ChatMessage>,
    socket: any,
    channel: any,
    status: String,
    inputValue: string,
    name: String,
}

export default class Chat extends React.Component<ChatProps, ChatState> {
  constructor(props:ChatProps) {
    super(props);

    /* Inject UserToken from outside */
    let socket:any = new Socket("/socket", {params: {token: window.userToken}});

    socket.connect();

    // Now that you are connected, you can join channels with a topic:
    let channel:any = socket.channel("room:" + props.channel, {})

    channel.on("chat_msg", this._handleChatMsg);

    channel.join()
      .receive("ok", this._connectionSuccessful)
      .receive("error", this._connectionFailed);

    this.state = { 
      messages: [],
      socket: socket,
      channel: channel,
      status: "Connecting",
      inputValue: "",
      name: props.name,
    }
  }

  private  _handleChatMsg = (payload:any) => {
    console.log("Received Chat Message: ", payload);
    let messages = this.state.messages;
    messages.push({name: payload.name, message: payload.message});

    this.setState({...this.state, messages: messages});
  }

  private _connectionSuccessful = (resp:any) => {
    console.log("Joined successfully", resp);
    this.setState({...this.state, status: "Connected" });
  }

  private _connectionFailed = (resp:any) => {
    console.log("Unable to join", resp);
    this.setState({...this.state, status: "Error" });
  }

  private _onInputChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const newInputValue = evt.currentTarget.value
    this.setState({...this.state, inputValue: newInputValue});
  }

  private _onSend = (evt: React.FormEvent) => {
    if (this.state.inputValue) {
      this.sendChatMsg();
    }
  }

  private _checkSubmit = (evt: React.KeyboardEvent<HTMLInputElement>) => {
    console.log(evt.charCode, evt.key);
    if (evt.key === 'Enter' && this.state.inputValue) {
      this.sendChatMsg();
    }
  }

  private sendChatMsg() {
    const message:ChatMessage = {name: this.state.name, message: this.state.inputValue};
    this.state.channel.push("chat_msg", message);
    this.setState({...this.state, inputValue: ""}); 
  }

  public render(): JSX.Element {
    return (
      <div className="w-full">
        <div className="fixed top-0 left-0 bg-gray-400 p-2 w-full shadow-md">
          <h2> ExChat </h2>
        </div>
        <div className="pl-2 pr-2 mt-12">
            {this.state.messages.map((chatMsg, idx) => 
                <Message key={idx} name={chatMsg.name} message={chatMsg.message} />
            )}
        </div>
        <div className="fixed bottom-0 left-0 flex bg-gray-400 p-2 w-full">
          <input className="chat-input" 
                 onChange={this._onInputChange}
                 value={this.state.inputValue}
                 onKeyPress={this._checkSubmit}
          />
          <button className="send-btn" onClick={this._onSend}>Send</button>
        </div>
      </div>
    )
  }
}