import * as React from 'react'

type LogonState = {
    username: string,
    channel: string,
    onLogon: Function
}

type LogonProps = {
    logonCallback: Function
}

export default class Root extends React.Component<LogonProps, LogonState> {
  constructor(props:any) {
    super(props);

    this.state = {
        username: "",
        channel: "",
        onLogon: props.logonCallback
    };
  }

  private _onUsernameChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
    const newUsername = evt.currentTarget.value;
    this.setState({...this.state, username: newUsername});
  }

  private _onChannelChange = (evt: React.ChangeEvent<HTMLInputElement>) => {
      const newChannel = evt.currentTarget.value;
      this.setState({...this.state, channel: newChannel});
  }

  private _onLogon = (evt: React.FormEvent) => {
    if (this.state.username) {
        if (this.state.channel) {
            this.state.onLogon(this.state.username, this.state.channel);
        } else {
            this.state.onLogon(this.state.username, "lobby");
        }
    }
  }

  public render():JSX.Element {
    return (
      <div className="flex flex-col bg-blue-200 justify-center h-screen" >
        <div className="w-1/3 bg-gray-200 shadow-lg rounded-lg px-8 py-6 mx-auto" >
          <label className="logon-label">Username</label>
          <input className="logon-input"
                 onChange={this._onUsernameChange}
                 value={this.state.username}
          />
          <label className="logon-label mt-4">Channel</label>
          <input className="logon-input"
                 onChange={this._onChannelChange}
                 value={this.state.channel}
          />
          <div className="flex flex-row justify-end">
            <button className="logon-btn" onClick={this._onLogon}>Logon</button>
          </div>
        </div>
      </div>
    );
  }
}